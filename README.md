#README#

1. I would Start a program by describing it in the beginning in the header. 
   For example: For program for subtracting two nos, It would be
                /* Program for subtracting two nos */


2. I would use first brace brackets at the left most corner and end at left most corner.
   For example: void main()
                {
                  Line 1..
                  Line 2..
                  ....;
                  ...;
                }


3. Would display inner brackets by using Tab. 
   For example: void main()
                {
                   ...;
                   ..;
                   for(i=0....)
                      {
                         cout<<i;
                      }
                   
                }


4. Would use Appropriate name for variable.
   For example: empName for emp Name.


5. Would use lower case for function or class name.
   For example: void total()
                class student



6. For combine function name or class name would use lower case for first name and Uppercase for            
   remaining name
   For example: void studentRollNo()
                class stevenName


7.Would use tab for every bracket so that it is understood properly.
  For example: if(...)
               {
                  if(..)
                    {

                    }
                  else( )
                    {
                    
                    }   
               }

8. Would declare same type of variable in one line.
   For example: All float type var in one line 
                float i, j, n;
                All int type var in one line
                int b, d;

8. Would use space between variable and variable type.
   For example: int i= a+b;
                float a;
                

9. Would use brackets and spaces for long function
   For example: sum = (a+b)/(c+d);

10.Would use single line comment by //
   For example: sub = a - b // sub of two nos 
   Would use /*   */  for many line comment
   For example: sub = ( a - b )/( d + c ) /* add two nos and then subtract from 
                                             it will give the value ....*/